# Configure Microphone

Problem 
=======
When you connect to a webconference system, you may hear sizzling because audio
gain is too high which causes saturation.

Solution
========
0. Unplug all earphones or standalone microphone from you computer
1. In a terminal, type `pavucontrol`, it should open a window
2. Go to "Input" menu and adjust volume of the input device to avoid sizzling
3. In another terminal type: `sudo alsactl store`
4. Now plug-in earphones with microphone or a standalone microphone
5. Go to `pavucontrol` window
6. Adjust volume of the input device to avoid sizzling
7. In another terminal type: `sudo alsactl store`
